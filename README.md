# rvm-travis-reader

### Usage

be inspired by the `demo.html` file

### Compiles and hot-reloads for development
```
yarn run serve
```

### Compiles and minifies for production
```
yarn build --target wc --name rvm-travis-reader ./src/components/RvmTravisReader.vue
```

### TODO:
   * build in CI
   * publish to npm
   * use unpkg for distribution